package se325.lab02.concert.services;

import java.net.URI;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import se325.lab02.concert.common.Config;
import se325.lab02.concert.domain.Concert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

    private Map<Long, Concert> concertDB = new ConcurrentHashMap<>();
    private AtomicLong idCounter = new AtomicLong();


    public Response retrieveConcert(long id, Cookie clientId) {
        NewCookie cookie = makeCookie(clientId);
        LOGGER.info("Retrieving concert with id: " + id);
        // Look up the Concert within the in-memory data structure
        final Concert concert = concertDB.get(id);
        if (concert == null) {
            // Return a HTTP 404 Response if the specified Concert is not found.
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        // Return a HTTP 200 Response with the Concert if found
        Response.ResponseBuilder builder = Response.ok(concert);
        if (cookie != null) {
            builder.cookie(cookie);
        }
        return builder.build();
    }


    public Response retrieveConcerts(long start, int size, Cookie clientId) {
        NewCookie cookie = makeCookie(clientId);

        // The Response object should store an ArrayList<Concert> entity. The
        // ArrayList can be empty depending on the start and size arguments,
        // and Concerts stored.
        //
        // Because of type erasure with Java Generics, any generically typed
        // entity needs to be wrapped by a javax.ws.rs.core.GenericEntity that
        // stores the generic type information. Hence to add an ArrayList as a
        // Response object's entity, you should use the following code:

        // Used to sort map by their key/id (long) in increasing order
        Map<Long, Concert> sortedMap = new TreeMap<Long, Concert>(concertDB);
        List<Concert> concertsInDb = new ArrayList<Concert>(sortedMap.values());

        // Finds the first concert to retrieve and its index in the list
        Concert startConcert = concertDB.get(start);
        int index = concertsInDb.indexOf(startConcert);

        List<Concert> concerts = new ArrayList<Concert>();
        for (int i = index; i < size + index; i++) {
            if (i < concertsInDb.size()) {
                concerts.add(concertsInDb.get(i));
            }
        }

        GenericEntity<List<Concert>> entity = new GenericEntity<List<Concert>>(concerts) {};
        Response.ResponseBuilder builder = Response.ok(entity);
        if (cookie != null) {
            builder.cookie(cookie);
        }
        return builder.build();
    }


    public Response createConcert(Concert concert, int size, Cookie clientId) {
        NewCookie cookie = makeCookie(clientId);

        // Reminder: You won't need to annotate the "concert" argument above - arguments without annotations are
        // assumed by JAX-RS to come from the HTTP request body.

        // Generate an ID for the new Concert, and store it in memory.
        concert.setId(idCounter.incrementAndGet());
        concertDB.put(concert.getId(), concert);

        LOGGER.debug("Created concert with id: " + concert.getId());

        Response.ResponseBuilder builder = Response.created(URI.create("/concerts/" + concert.getId()));
        if (cookie != null) {
            builder.cookie(cookie);
        }
        return builder.build();
    }


    public Response deleteAllConcerts(Cookie clientId) {
        NewCookie cookie = makeCookie(clientId);

        concertDB.clear();
        idCounter = new AtomicLong();

        Response.ResponseBuilder builder = Response.noContent();
        if (cookie != null) {
            builder.cookie(cookie);
        }
        return builder.build();
    }

    /**
     * Helper method that can be called from every service method to generate a
     * NewCookie instance, if necessary, based on the clientId parameter.
     *
     * @param clientId the Cookie whose name is Config.CLIENT_COOKIE, extracted
     *                 from a HTTP request message. This can be null if there was no cookie
     *                 named Config.CLIENT_COOKIE present in the HTTP request message.
     * @return a NewCookie object, with a generated UUID value, if the clientId
     * parameter is null. If the clientId parameter is non-null (i.e. the HTTP
     * request message contained a cookie named Config.CLIENT_COOKIE), this
     * method returns null as there's no need to return a NewCookie in the HTTP
     * response message.
     */
    private NewCookie makeCookie(Cookie clientId) {
        NewCookie newCookie = null;

        if (clientId == null) {
            newCookie = new NewCookie(Config.CLIENT_COOKIE, UUID.randomUUID().toString());
            LOGGER.info("Generated cookie: " + newCookie.getValue());
        }

        return newCookie;
    }
}
